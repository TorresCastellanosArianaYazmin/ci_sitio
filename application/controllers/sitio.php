<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitio extends CI_Controller
{
  private $datos;
   
  public function __construct()
  {
    parent::__construct();
    $this->datos = array(
      'cuerpo' => '',
    );
  }
  
  public function index()
  {
    $this->datos['cuerpo'] = 'sitio/vista_index.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  
  public function menu()
  {
    $this->datos['cuerpo'] = 'sitio/vista_menu.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  public function sucursales()
  {
    $this->datos['cuerpo'] = 'sitio/vista_sucursales.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  public function especialestem()
  {
    $this->datos['cuerpo'] = 'sitio/vista_especialestem.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  
  public function eventos()
  {
    $this->datos['cuerpo'] = 'sitio/vista_eventos.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  
  public function banquetes()
  {
    $this->datos['cuerpo'] = 'sitio/vista_banquetes.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  public function reservaciones()
  {
    $this->datos['cuerpo'] = 'sitio/vista_reservaciones.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }
  public function promociones()
  {
    $this->datos['cuerpo'] = 'sitio/vista_promociones.php';
    $this->load->view('sitio/vista_base.php', $this->datos);
  }

}
